"""
Escriba un ciclo WHILE que lea números hasta que el usuario lo desee y los agregue a un
diccionario, posteriormente imprima los elementos de dicho diccionario, el key del
diccionario de ser generado aleatoriamente (Tema: ciclo While, Diccionario)
"""
import random as rd
diccionario=dict()
opcion = 's'
num = 0
while opcion == 's':
    num += 1
    a = rd.randint(1,100)
    diccionario.update({a:num})

    print(diccionario)
    opcion=input("Desea continuar? (s/n) : ")


elemtos=diccionario.items()

for a,num in elemtos:
    print("La clave es : ",a,"----->",num," y este es el valor.")
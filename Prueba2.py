"""
Cree una matriz de 3 x 3 y rellene los elementos con números aleatorios según se indica
(Tema: Matrices y uso de Random) valor 20 puntos
• La columna #1 debe tener números entre 10 y 20
• La columna #2 debe tener números entre 21 y 30
• La columna #3 debe tener números entre 31 y 40
• La posición central debe ser un 99
"""
import random as rd
matriz = []

filas = 3
columnas = 3

for i in range(filas):
    matriz.append([0] * columnas)

for i in range(filas):
    for j in range(columnas):
        a = rd.randint(10, 20)
        b= rd.randint(21, 30)
        c= rd.randint(31, 40)
        if j == 0:
            matriz[i][j] =a
        elif j == 1:
            matriz[i][j] = b
        elif j== 2:
            matriz[i][j] = c
matriz[1][1] = 99



for i in matriz:
    print(i)
